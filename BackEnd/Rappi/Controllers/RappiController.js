﻿
rappiApp.controller('RappiController', ['$scope', RappiController]);
var matrix = [];
function RappiController($scope) {
    $scope.step = 1;
    $scope.nTestCases = 0;//number of test casese
    $scope.N = 0;//dimensions
    $scope.M = 0;//number of operations
    $scope.countM = 0;
    $scope.countTestCases = 0;
    $scope.queryType = 0;//1. Update x y z N  2. Query x1 y1 z1 x2 y2 z2
    $scope.outPut = "";

    function rangeValidations(val, d, h,message) {
        if (d > val || val > h || val == null) {
            Materialize.toast(d+" <= "+message+" <= "+h, 3000);
            return true;
        } else {
            return false;
        }
    }

    $scope.step1 = function (number) {
        if (rangeValidations(number, 1, 50, "TestCases"))
            return false;

        $scope.nTestCases = number;
        $scope.step++;
    }


    $scope.step2 = function (step2) {

        if (rangeValidations(step2.N, 1, 100, "TopMatrix"))
            return false;

        if (rangeValidations(step2.M, 1, 1000, "Number of operations"))
            return false;

        $scope.N = step2.N;
        $scope.M = step2.M;
        $scope.step++;
        setZeroMatriz();
    }

    $scope.update = function (cords1) {

        if (rangeValidations(cords1.x, 1, $scope.N, "x"))
            return false;
        if (rangeValidations(cords1.y, 1, $scope.N, "y"))
            return false;
        if (rangeValidations(cords1.z, 1, $scope.N, "z"))
            return false;
        if (rangeValidations(cords1.v, Math.pow(-10, 9), Math.pow(10, 9), "Valor"))
            return false;

        matrix[cords1.x - 1][cords1.y - 1][cords1.z - 1] = cords1.v;
        Materialize.toast("Added Number", 3000);
        validation();
        cords1.x = "";
        cords1.y = "";
        cords1.z = "";
        cords1.v = "";
    }

    $scope.query = function (cords1, cords2) {

        if (rangeValidations(cords1.x, 1, $scope.N, "x1") || rangeValidations(cords2.x, cords1.x, $scope.N, "x2"))
            return false;
        if (rangeValidations(cords1.y, 1, $scope.N, "y1") || rangeValidations(cords2.y, cords1.y, $scope.N, "y2"))
            return false;
        if (rangeValidations(cords1.z, 1, $scope.N, "z1") || rangeValidations(cords2.z, cords1.z, $scope.N, "z2"))
            return false;


        var suma = 0;
        for (var cx = cords1.x - 1; cx < cords2.x; cx++) {
            for (var cy = cords1.y - 1; cy < cords2.y; cy++) {
                for (var cz = cords1.z - 1; cz < cords2.z; cz++) {
                    if (matrix[cx][cy][cz] > 0) {
                        suma += parseInt(matrix[cx][cy][cz]);
                    }
                }
            }
        }

        $scope.outPut += suma + "<br/>";

        validation();
        cords1.x = "";
        cords1.y = "";
        cords1.z = "";

        cords2.x = "";
        cords2.y = "";
        cords2.z = "";
    }

    function validation() {

        $scope.countM++;
        if ($scope.countM == $scope.M) {
            $scope.countTestCases++;
            if ($scope.countTestCases == $scope.nTestCases) {
                $scope.step = 4;
                Materialize.toast("GAME OVER", 3000);
            } else {
                setZeroMatriz();
                $scope.step = 2;
                $scope.countM = 0;
            }
        }
    };

    function setZeroMatriz() {

        matrix = new Array();
        for (var cy = 0; cy < $scope.N; cy++) {
            var yArray = new Array();
            for (var cz = 0; cz < $scope.N; cz++) {
                var zArray = new Array();
                for (var cw = 0; cw < $scope.N; cw++) {
                    zArray.push(0);
                }
                yArray.push(zArray);
            }
            matrix.push(yArray);
        }
    };


}
